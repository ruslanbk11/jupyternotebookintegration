from IPython.core.interactiveshell import InteractiveShell
import nbformat
import yaml
import dill
from matplotlib.pyplot import Figure
from markdown_it import MarkdownIt
import os
import re


class NotebookPipeline:
    def __init__(self, stage_name, pipeline_config, compare):
        split_params = pipeline_config.split('.')
        notebook_name = split_params[0] + '.ipynb'
        self.notebook = nbformat.read(notebook_name, nbformat.NO_CONVERT)
        self.pipeline_name = split_params[1]
        self.runner = NotebookRunner(stage_name, self.notebook, compare, self.get_context_from_notebook())

    def __repr__(self):
        return str(self.__dict__)

    def get_context_from_notebook(self):
        md = MarkdownIt()
        for cell in self.notebook.cells:
            if cell.cell_type == 'markdown':
                parsed_cell = md.parse(cell.source)
                for token in parsed_cell:
                    token_dict = token.as_dict()
                    if token_dict['type'] == 'fence' and token_dict['tag'] == 'code' and token_dict['info'] == 'yaml':
                        if '%mldev nb_context' in token_dict['content']:
                            return yaml.load(token_dict['content'], Loader=yaml.SafeLoader)
        return {}

    def run(self):
        self.runner.run_pipeline(self.pipeline_name)

    @classmethod
    def from_yaml(cls, loader, node):
        return NotebookPipeline('test_stage', node.value, 'True')


class NotebookRunner:
    user_ns = {}
    ip_shell = InteractiveShell(user_ns=user_ns)

    def __init__(self, stage_name, notebook, compare, nb_context):
        self.stage_name = stage_name
        self.nb_context = nb_context
        self.notebook = notebook
        self.compare = compare
        self.cells = self.get_mldev_code_cells()

    def get_mldev_code_cells(self):
        cell_sources = {}
        started_block = ''
        for cell in self.notebook.cells:
            if cell.cell_type == 'code':
                if started_block:
                    cell_sources[started_block].append(cell.source)
                for line in cell.source.split('\n'):
                    cell_names = re.findall(r'#\s*%mldev\s+(\w+\s*\w+)', line)
                    if len(cell_names):
                        cell_name = cell_names[0].split()
                        if len(cell_name) == 1:
                            cell_sources[cell_name[0]] = [cell.source]
                        else:
                            if cell_name[1] == 'start':
                                started_block = cell_name[0]
                                cell_sources[started_block] = [cell.source]
                            elif cell_name[1] == 'end':
                                started_block = ''
        return cell_sources

    def get_cell_code(self, cell_name):
        return self.cells[cell_name]

    def run_cell_with_deps(self, cell_name):
        if 'notebook_deps' in self.nb_context.keys():
            if cell_name in self.nb_context['notebook_deps'].keys():
                for cell in self.nb_context['notebook_deps'][cell_name].split():
                    self.run_cell_with_deps(cell)
        for code in self.get_cell_code(cell_name):
            cell_result = self.ip_shell.run_cell(code)
            if cell_result.error_in_exec is not None:
                cell_result.raise_error()

    def get_results(self):
        saving_vars = self.nb_context['results']
        vars_dict = {}
        for var in saving_vars:
            if type(self.user_ns[var]) != Figure:
                vars_dict[var] = self.user_ns[var]
        return vars_dict

    def save_results(self):
        if 'results' not in self.nb_context.keys():
            return
        os.makedirs('results', exist_ok=True)
        vars_dict = self.get_results()
        if len(list(vars_dict.keys())) != 0:
            with open('./results/{}.pickle'.format(self.stage_name), mode='wb') as f:
                dill.dump(vars_dict, f)
        saving_vars = self.nb_context['results']
        for var in saving_vars:
            if type(self.user_ns[var]) == Figure:
                self.user_ns[var].savefig('./results/{}.png'.format(var))

    def compare_results(self):
        if 'results' not in self.nb_context.keys():
            return
        vars_dict = self.get_results()
        with open('./results/{}.pickle'.format(self.stage_name), mode='rb') as f:
            saved_results = dill.load(f)
        for key in vars_dict.keys():
            if saved_results[key] != vars_dict[key]:
                print('Variables for key {} are not equal:\nsaved: {} \n new: {}'.format(key, saved_results[key], vars_dict[key]))
            else:
                print('equal:', key, saved_results[key], vars_dict[key])

    def run_pipeline(self, seq_name):
        if seq_name == 'all_cells':
            for cell in self.notebook.cells:
                if cell.cell_type == 'code':
                    self.ip_shell.run_cell(cell.source)
        else:
            seq = self.nb_context[seq_name]
            for cell_name in seq:
                self.run_cell_with_deps(cell_name)
        if self.compare == 'True':
            self.compare_results()
        else:
            self.save_results()


yaml.add_constructor('!nb_pipeline', NotebookPipeline.from_yaml)
with open('./experiment.yml') as _f:
    experiment = yaml.load(_f, yaml.FullLoader)
pipeline_runner = experiment['pipeline_runner']
pipeline_runner.run()
